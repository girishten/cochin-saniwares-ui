<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Password extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    $this->load->library('session');
    $this->load->library('layout');
    $this->load->model('Admin_m');
    }
    function index()
    {
//        $result['query']=$this->Admin_m->select_pass();
        $this->layout->view('design/check');
    }
    function change()
    {
        $pword=$this->input->post('pword');
        $cword=$this->input->post('cword');
        if($pword==$cword)
        {
            $mdpass=  md5($pword);
            $update="UPDATE `admin` SET `Vpass`='$mdpass'";
            $this->Admin_m->Query1($update);
            $this->session->set_flashdata('success','Password changed Successfully');
            redirect(Password);
        }
    }
}
