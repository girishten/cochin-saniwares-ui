<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class TilesandsurfacesController extends CI_Controller {
public function __construct() {
        parent::__construct();
        $this->load->model('tilesandsurfaces');
        $this->load->model('Admin_m');
        $this->load->library('session');
        }
    public function index() { 

        $data["tilesandsurfacess"] = $this->tilesandsurfaces->getAll();
        $this->layout->view('tilesandsurfaces/manage-tilesandsurfaces', $data);
    
    }
    public function manageTilesandsurfaces() { 

        $data["tilesandsurfacess"] = $this->tilesandsurfaces->getAll();
        $this->layout->view('tilesandsurfaces/manage-tilesandsurfaces', $data);
    
    }
    public function addTilesandsurfaces() {

        $this->layout->view('tilesandsurfaces/add-tilesandsurfaces');

    }
    public function addTilesandsurfacesPost() {
                        $data['category'] = $this->input->post('category');
                                $data['description'] = $this->input->post('description');
                                if ($_FILES['image']['name']) { 
            $data['image'] = $this->doUpload('image');
         } 
                        $this->tilesandsurfaces->insert($data);
        $this->session->set_flashdata('success', 'Tilesandsurfaces added Successfully');
        redirect('manage-tilesandsurfaces');
    }
    public function editTilesandsurfaces($tilesandsurfaces_id) {
        $data['tilesandsurfaces_id'] = $tilesandsurfaces_id;
        $data['tilesandsurfaces'] = $this->tilesandsurfaces->getDataById($tilesandsurfaces_id);
        $this->layout->view('tilesandsurfaces/edit-tilesandsurfaces', $data);
    }
    public function editTilesandsurfacesPost() {

        $tilesandsurfaces_id = $this->input->post('tilesandsurfaces_id');
        $tilesandsurfaces = $this->tilesandsurfaces->getDataById($tilesandsurfaces_id);
                        $data['category'] = $this->input->post('category');
                        $data['description'] = $this->input->post('description');
                        if ($_FILES['image']['name']) { 
            $data['image'] = $this->doUpload('image');
            unlink('./uploads/tilesandsurfaces/'.$tilesandsurfaces[0]->image);
        } 
                $edit = $this->tilesandsurfaces->update($tilesandsurfaces_id,$data);
        if ($edit) {
            $this->session->set_flashdata('success', 'Tilesandsurfaces Updated');
            redirect('manage-tilesandsurfaces');
        }
    }
    public function viewTilesandsurfaces($tilesandsurfaces_id) {
        $data['tilesandsurfaces_id'] = $tilesandsurfaces_id;
        $data['tilesandsurfaces'] = $this->tilesandsurfaces->getDataById($tilesandsurfaces_id);
        $this->layout->view('tilesandsurfaces/view-tilesandsurfaces', $data);
    }
    public function deleteTilesandsurfaces($tilesandsurfaces_id) {
        $delete = $this->tilesandsurfaces->delete($tilesandsurfaces_id);
        $this->session->set_flashdata('success', 'tilesandsurfaces deleted');
        redirect('manage-tilesandsurfaces');
    }
    public function changeStatusTilesandsurfaces($tilesandsurfaces_id) {
        $edit = $this->tilesandsurfaces->changeStatus($tilesandsurfaces_id);
        $this->session->set_flashdata('success', 'tilesandsurfaces '.$edit.' Successfully');
        redirect('manage-tilesandsurfaces');
    }
    function doUpload($file) {
        $config['upload_path'] = './uploads/tilesandsurfaces';
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload($file))
            {
              $error = array('error' => $this->upload->display_errors());
              $this->load->view('upload_form', $error);
            }
            else
            {
              $data = array('upload_data' => $this->upload->data());
              return $data['upload_data']['file_name'];
            }
        }
       function trip_pics($id)
 {
     $result['query']=$this->Admin_m->select_id('tilesandsurfaces',$id);
     $result['trippics']=$this->Admin_m->select_cid('trippics',$id);

     $this->layout->view('trip_pics',$result);
 }
 function delete_pics($id,$pid)
 {
     $result['query']=$this->Admin_m->select_id('tilesandsurfaces',$id);
     $this->Admin_m->delete('trippics',$pid);
     $result['trippics']=$this->Admin_m->select_cid('trippics',$id);

     $this->layout->view('trip_pics',$result);
 }
 function add_trip_pics($id)
 {if($this->input->post('submit'))
     {
     $name_array = array();
		$count = count($_FILES['userfile']['size']);
		foreach($_FILES as $key=>$value)
		for($s=0; $s<=$count-1; $s++) {
		$_FILES['userfile']['name']=$value['name'][$s];
		$_FILES['userfile']['type']    = $value['type'][$s];
		$_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
		$_FILES['userfile']['error']       = $value['error'][$s];
		$_FILES['userfile']['size']    = $value['size'][$s];   
		    $config['upload_path'] = './uploads/products';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '100000';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
		$this->load->library('upload', $config);
                $this->upload->initialize($config);
		$this->upload->do_upload();
		$data = $this->upload->data();
		$name_array[] = $data['file_name'];
                $image=$data['file_name'];
		//print_r($name_array);die;
                if($image!='')
                {
                $this->load->database();
                $this->db->query("Insert into trippics(cid,image)values('$id','$image')");
                }}
//			$names= implode(',', $name_array);
//			$this->load->database();
//			$db_data = array('id'=> NULL,
//							 'image'=> $names);
		
//			print_r($names);
 } redirect('TilesandsurfacesController'); }
 function update_test($id){
     $sort=$this->input->post('sort');
     
     $this->Admin_m->update_test($sort,$id);
     redirect('Home/Destinations','refresh');
 }
    
}