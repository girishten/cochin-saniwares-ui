<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->helper(array('form','url','security'));
   $this->load->library('session');
    $this->load->library('layout');
    $this->load->model('Admin_m');
    $this->load->model('Package_m');
    $this->load->model('Testimonial_m');
    $this->load->library('upload');
    $this->load->helper('url');
 }

 function index()
 {
//     $this->load->view('welcome_message');
//     print_r('ddd');die();
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['Vuser'];
     $data['query']=$this->Admin_m->get_cat();
     $this->layout->view('design/courses', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('Admin', 'refresh');
   }
 }

 function logout()
 {
     //print_r('sss');die();
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('Home', 'refresh');
 }
 function video()
 {
     $this->layout->view('video');
 }
 function add_video()
 {
     $n=$this->input->post('name');
     $video=$this->input->post('video');
     $this->Admin_m->add_video($video,$n);
     $result['query']=$this->Admin_m->view_video();
     $this->layout->view('view_video',$result);
 }
 function view_video()
 {
     $result['query']=$this->Admin_m->view_video();
     $this->layout->view('view_video',$result);
 }
 function delete_video($id)
 {
     $this->Admin_m->delete_video($id);
     redirect('home/view_video');
 }
 function edit_video($id)
 {
     $result['query']=$this->Admin_m->view_video_id($id);
     $this->layout->view('edit_video',$result);
 }
 function update_video($id)
 {
    $n=$this->input->post('name');
     $video=$this->input->post('src');
     $this->Admin_m->update_video($video,$n,$id);
     redirect('home/view_video');
 }
 function add_package_type()
 {
     if($this->input->post('submit')){
        $type=$this->input->post('type');
        $query="INSERT into package_type(type)values('$type')";
         $this->Package_m->insert_parameters($query);
         redirect('home/add_package_type');
     }
     $table="package_type";
     $result['query']=$this->Package_m->Select_Query($table);
     $this->layout->view('package/package_type',$result);
     
 }
 function delete_package_type($id,$pid)
 {
     $this->Package_m->delete_query($id);
     redirect('home/add_package_type');
 }
 
 function delete_package_type1($id)
 {
     $this->Package_m->delete_query($id);
     redirect('home/add_package_type');
 }
 
 
 function Destinations()
 {
     if($this->input->post('submit'))
     {
         $main= $this->input->post('main');
        $name= $this->input->post('name');
        $destinations= $this->input->post('destinations');
        $gender=$this->input->post('gender');
        $place=$this->input->post('place');
         
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '10000';
        $config['max_width'] = '10240';
        $config['max_height'] = '7680';
        
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('userfile')) {
            

            
            $error = array('error' => $this->upload->display_errors());

            $this->layout->view('home_page', $error);
            
        } else {
            

            
            $data = array('upload_data' => $this->upload->data());
            $data1 = $this->upload->data();
            $image = $data1['file_name'];
        
//        $query="INSERT into package(name,places,overview,image,days,status)values('$name','$place','$overview','$image','$days','1')";
         $this->Admin_m->Insert_destinations($name,$destinations,$image,$gender,$place,$main);
         redirect('home/Destinations');
        }
     }
     $result['main']=$this->Admin_m->select('dest_cat');
     $result['query']=$this->Admin_m->select_destinations();
     $this->layout->view('Destinations',$result);
 }
 function delete_Destinations($id,$image)
 {
     $file="uploads/$image";
     if(is_readable($file))
     {
          unlink($file);
     }
         
//     print_r($file);die;
     $table="destinations";
     $this->Package_m->delete_query1($table,$id);
     redirect('home/Destinations');
 }
 function edit_Destinations($id)
 {
     $result['query']=$this->Testimonial_m->select_id('destinations',$id);
     $this->layout->view('edit_testimonial',$result);
 }
 function trip_pics($id)
 {
     $result['query']=$this->Admin_m->select_destinations($id);

     $this->layout->view('trip_pics',$result);
 }
 function add_trip_pics($id)
 {if($this->input->post('submit'))
     {
     $name_array = array();
		$count = count($_FILES['userfile']['size']);
		foreach($_FILES as $key=>$value)
		for($s=0; $s<=$count-1; $s++) {
		$_FILES['userfile']['name']=$value['name'][$s];
		$_FILES['userfile']['type']    = $value['type'][$s];
		$_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
		$_FILES['userfile']['error']       = $value['error'][$s];
		$_FILES['userfile']['size']    = $value['size'][$s];   
		    $config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '100000';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
		$this->load->library('upload', $config);
                $this->upload->initialize($config);
		$this->upload->do_upload();
		$data = $this->upload->data();
		$name_array[] = $data['file_name'];
                $image=$data['file_name'];
		//print_r($name_array);die;
                if($image!='')
                {
                $this->load->database();
                $this->db->query("Insert into trippics(cid,image)values('$id','$image')");
                }}
//			$names= implode(',', $name_array);
//			$this->load->database();
//			$db_data = array('id'=> NULL,
//							 'image'=> $names);
		
//			print_r($names);
 } redirect('Home/Destinations'); }
  
}

?>