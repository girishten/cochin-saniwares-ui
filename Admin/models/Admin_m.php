<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin_m extends CI_Model
{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->database();
        
    }
    function Query1($query)
    {
      $this->db->query($query);
      
    }
    function select($query1)
    {
       $this->db->select('*');
       $query=$this->db->get($query1);
       return $query->result(); 
    }
    function do_upload($image,$name,$cat)
    {
        
        $this->db->set('name',$name);
         $this->db->set('cid',$cat);
        $this->db->set('src',$image);
        $this->db->insert('images');
    }
    function view($limit, $start)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get("images");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
    public function record_count() {
        return $this->db->count_all("images");
    }
    function view1($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        $query=$this->db->get('images');
        return $query->result();
    }
    function img_update($image,$name,$id)
    {
        $this->db->set('coursename',$name);
        $this->db->set('image',$image);
        $this->db->where('id',$id);
        $this->db->update('images');

    }
    function login($username, $password)
    {
        $this -> db -> select('Vid, Vuser, Vpass');
        $this -> db -> from('admin');
        $this -> db -> where('Vuser', $username);
        $this -> db -> where('Vstatus','1');
        $this -> db -> where('Vpass', MD5($password));
        $this -> db -> limit(1);
        $query = $this -> db -> get();
   

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 function delete_image($id)
 {
     $this->db->select('*');
     $this->db->where('id',$id);
     $this->db->delete('images');
     
 }
 function add_cat($cat)
 {
     $this->db->set('name',$cat);
     $this->db->insert('category');
 }
 function get_cat()
 {
     $this->db->select('*');
     $query=$this->db->get('category');
     return $query->result();
 }
 function delete($table,$pid){
     $this->db->where('id',$pid);
     $this->db->delete($table);
}
 function add_video($video,$n)
 {
     $this->db->set('name',$n);
     $this->db->set('src',$video);
     $this->db->insert('vedio');
 }
 function view_video()
 {
     $this->db->select('*');
     $query=$this->db->get('vedio');
     return $query->result();
 }
 function view_video_id($id)
 {
     $this->db->select('*');
     $this->db->where('id',$id);
     $query=$this->db->get('vedio');
     return $query->result();
 }
 function delete_video($id)
 {
     $this->db->select('*');
     $this->db->where('id',$id);
     $this->db->delete('vedio');
     
 }
 function update_video($video,$n,$id)
 {
     $this->db->set('name',$n);
     $this->db->set('src',$video);
     $this->db->where('id',$id);
     $this->db->update('vedio');
 }
 function delete1($ids){
        $ids = $ids;
       $count = 0;
        foreach ($ids as $id){
           $did = intval($id).'<br>';
            $this->db->where('id', $did);
            $this->db->delete('multi_del');  
            $count = $count+1;
       }
       
        echo'<div class="alert alert-success" style="margin-top:-17px;font-weight:bold">
            '.$count.' Item deleted successfully
            </div>';
        $count = 0;
}
function Insert_destinations($name,$destinations,$image,$gender,$place,$main)
{
     $this->db->set('name',$name);
//     $this->db->set('places',$place);
//     $this->db->set('main',$main);
     $this->db->set('destinations',$destinations);
     $this->db->set('image',$image);
//     $this->db->set('gender',$gender);
     $this->db->insert('destinations');
     
}
function select_product()
{
     $this->db->select('*');
     $query=$this->db->get('tilesandsurfaces');
     return $query->result();
     
}
function select_trippics_id($id)
{
     $this->db->select('*');
     $this->db->where('id',$id);
     $query=$this->db->get('trippics');
     return $query->result();
     
}
function select_cid($table,$id)
    {
        $this->db->where('cid',$id);
        $query = $this->db->get($table);
        return $query->result();
    }
    function select_id($table,$id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get($table);
        return $query->result();
    }
function select_trippics_id1($id)
{
     $this->db->select('*');
     $this->db->where('cid',$id);
     $query=$this->db->get('trippics');
     return $query->result();
     
}
 
}