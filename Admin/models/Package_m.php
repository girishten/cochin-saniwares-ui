<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Package_m extends CI_Model
{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->database();
    }
    function insert_parameters($query)
    {
       $this->db->query($query);
        
    }
    function Select_Query($query1)
    {
       $this->db->select('*');
       $query=$this->db->get($query1);
       return $query->result(); 
    }
    function delete_query($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('package_type');
    }
    function delete_query1($table,$id)
    {
        $this->db->where('id',$id);
        $this->db->delete($table);
    }
    function Insert_package($name,$place,$overview,$days,$image,$cat,$price,$checkin,$checkout,$sort)
    {
        $this->db->set('name',$name);
        $this->db->set('places',$place);
        $this->db->set('overview',$overview);  
        $this->db->set('price',$price);
        $this->db->set('type',$cat);
        $this->db->set('image',$image);
        $this->db->set('days',$days);
        $this->db->set('checkin',$checkin);
        $this->db->set('checkout',$checkout);
        $this->db->set('sort',$sort);
        $this->db->set('status','1');
        $this->db->insert('package');
        
    }
    function Insert_package_cat($name,$image)
    {
        $this->db->set('name',$name);
        $this->db->set('src',$image);
        $this->db->set('status','1');
        $this->db->insert('package_type');
        
    }
    function Insert_dest_cat($name,$image,$des)
    {
        $this->db->set('name',$name);
        $this->db->set('src',$image);
        $this->db->set('details',$des);
        $this->db->set('status','1');
        $this->db->insert('dest_cat');
        
    }
    function Insert_travel($name,$day,$place,$description)
    {
        $this->db->set('pid',$name);
//        $this->db->set('place',$place);
        $this->db->set('description',$description);
        $this->db->set('day',$day);
        $this->db->set('status','1');
        $this->db->insert('travel');
        
    }
    function Insert_travel1($name,$day,$image,$description)
    {
        $this->db->set('pid',$name);
        $this->db->set('image',$image);
        $this->db->set('description',$description);
        $this->db->set('day',$day);
        $this->db->set('status','1');
        $this->db->insert('travel');
        
    }
    function select_package_id($table,$id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        $query=$this->db->get($table);
        return $query->result(); 
    }
    function select_package_pid($table,$id)
    {
        $this->db->select('*');
        $this->db->where('pid',$id);
        $query=$this->db->get($table);
        return $query->result(); 
    }
    function Update_package($name,$place,$overview,$days,$image,$cat,$id,$price,$sort)
    {
        $this->db->set('name',$name);
        $this->db->set('places',$place);
        $this->db->set('overview',$overview);
        $this->db->set('price',$price);
        $this->db->set('type',$cat);
        $this->db->set('image',$image);
        $this->db->set('sort',$sort);
        $this->db->set('days',$days);
        $this->db->where('id',$id);
        $this->db->update('package');
    }
    function Update_package_cat($name,$image,$id)
    {
        $this->db->set('name',$name);
        $this->db->set('src',$image);
        $this->db->where('id',$id);
        $this->db->update('package_type');
    }
    function Update_dest_cat($name,$image,$details,$id)
    {
        $this->db->set('name',$name);
        $this->db->set('src',$image);
        $this->db->set('details',$details);
        $this->db->where('id',$id);
        $this->db->update('dest_cat');
    }
    function Update_package_without_image($name,$place,$overview,$days,$cat,$id,$price,$sort)
    {
        $this->db->set('name',$name);
        $this->db->set('places',$place);
        $this->db->set('overview',$overview);
        $this->db->set('price',$price);
        $this->db->set('type',$cat);
        $this->db->set('sort',$sort);
        $this->db->set('days',$days);
        $this->db->where('id',$id);
        $this->db->update('package');
    }
    function Update_package_cat_without_image($name,$id)
    {
        $this->db->set('name',$name);
        $this->db->where('id',$id);
        $this->db->update('package_type');
    }
    function Update_dest_cat_without_image($name,$details,$id)
    {
        $this->db->set('name',$name);
        $this->db->set('details',$details);
        $this->db->where('id',$id);
        $this->db->update('dest_cat');
    }
    function Insert_taxi($name,$image)
    {
        $this->db->set('name',$name);       
        $this->db->set('image',$image);
        $this->db->set('status','1');
        $this->db->insert('taxi');
        return $this->db->insert_id();;
        
    }
    function Insert_taxi_ac($ac100,$acextra,$acnight,$id)
    {
        $this->db->set('ac100',$ac100);       
        $this->db->set('acextra',$acextra);
        $this->db->set('acnight',$acnight);
        $this->db->set('cid',$id);
        $this->db->insert('taxi_ac');
        
        
    }
    function Insert_taxi_nac($nac100,$nacextra,$nacnight,$id)
    {
        $this->db->set('nac100',$nac100);
        $this->db->set('nacextra',$nacextra);
        $this->db->set('nacnight',$nacnight);
        $this->db->set('cid',$id);
        $this->db->insert('taxi_nac');
    }
    function active_deactive($table,$status,$id)
    {
        $this->db->set('status',$status);
        $this->db->where('id',$id);
        $this->db->update($table);
                
    }
    function delete_query_pid($table,$id)
    {
        $this->db->where('pid',$id);
        $this->db->delete($table);
    }
    
}