<?php

class Tilesandsurfaces extends CI_Model {
    public function getAll() {
        return $this->db->get('tilesandsurfaces')->result();
    }
    public function insert($data) {
        $this->db->insert('tilesandsurfaces', $data);
        return $this->db->insert_id();
    }
    public function getDataById($id) {
        $this->db->where('id', $id);
        return $this->db->get('tilesandsurfaces')->result();
    }
    public function update($id,$data) {
        $this->db->where('id', $id);
        $this->db->update('tilesandsurfaces', $data);
        return true;
    }
    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tilesandsurfaces');
        return true;
    }
    public function changeStatus($id) {
        $table=$this->getDataById($id);
             if($table[0]->status==0)
             {
                $this->update($id,array('status' => '1'));
                return "Activated";
             }else{
                $this->update($id,array('status' => '0'));
                return "Deactivated";
             }
    }

}