<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li class="active" ><a href="<?php echo site_url(); ?>/manage-tilesandsurfaces">Manage Products</a></li>
        <li><a href="<?php echo site_url(); ?>/add-tilesandsurfaces">Add Products</a></li>
      </ul>
  </div>
</nav>


  <h2>Manage Products</h2>

  <?php if($this->session->flashdata('success')){ ?>
  <div class="alert alert-success">
                    <strong><span class="glyphicon glyphicon-ok"></span>   <?php echo $this->session->flashdata('success'); ?></strong>
                </div>
  <?php } ?>


  
<?php if(!empty($tilesandsurfacess)) {?>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>SL No</th>
        <th>category</th>
       <th>Actions</th>
       <th>Add Photos</th>
      </tr>
    </thead>
    <tbody>
    <?php $i=1; foreach($tilesandsurfacess as $tilesandsurfaces) { ?>
      <tr>
        <td> <?php echo $i; ?> </td>
        <td> <a href="<?php echo site_url()?>/view-tilesandsurfaces/<?php echo $tilesandsurfaces->id?>" > <?php echo $tilesandsurfaces->category ?> </a> </td>

        <td>
        <a href="<?php echo site_url()?>/change-status-tilesandsurfaces/<?php echo $tilesandsurfaces->id ?>" > <?php if($tilesandsurfaces->status==0){ echo "Activate"; } else { echo "Deactivate"; } ?></a>
        <a href="<?php echo site_url()?>/edit-tilesandsurfaces/<?php echo $tilesandsurfaces->id?>" >Edit</a>
        <a href="<?php echo site_url()?>/delete-tilesandsurfaces/<?php echo $tilesandsurfaces->id?>" onclick="return confirm('are you sure to delete')">Delete</a></td><td>
        <a href="<?php echo site_url('TilesandsurfacesController/trip_pics/'.$tilesandsurfaces->id); ?>">Add Pics</a>

        </td>

      </tr>
    <?php $i++; } ?>
    </tbody>
  </table>
  <?php } else {?>
  <div class="alert alert-info" role="alert">
                    <strong>No Products Found!</strong>
                </div>
  <?php } ?>
</body>
</html>