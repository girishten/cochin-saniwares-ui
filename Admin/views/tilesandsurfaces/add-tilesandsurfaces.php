<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script type="text/javascript" src="<?php echo base_url(); ?>assets1/ckeditor/ckeditor.js" ></script>
        
</head>
<body>



<nav class="navbar navbar-inverse">
  <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo site_url(); ?>/manage-tilesandsurfaces">Manage Products</a></li>
        <li class="active" ><a href="<?php echo site_url(); ?>/add-tilesandsurfaces">Add Products</a></li>
      </ul>
  </div>
</nav>




  <h2>Add PRODUCTS</h2>  
<form role="form" method="post" action="<?php echo site_url()?>/add-tilesandsurfaces-post"  enctype="multipart/form-data" >
    <div class="form-group">
<label for="category">Category:</label>
<select class="form-control" id="category" name="category" required>
    <option value="">Select Category</option>
    <option disabled>---TILES & SURFACES---</option>
<option value="INDOOR WALL TILES">INDOOR WALL TILES</option>
<option value="INDOOR FLOOR TILES">INDOOR FLOOR TILES</option>
<option value="OUTDOOR WALL TILES">OUTDOOR WALL TILES</option>
<option value="OUTDOOR FLOOR TILES">OUTDOOR FLOOR TILES</option>
<option value="LAWN & TERRACE TILES">LAWN & TERRACE TILES</option>
<option value="WOODEN FLOORING">WOODEN FLOORING</option>
<option disabled></option>
<option disabled>---BATHROOM ACCESSORIES---</option>
<option value="CLOSETS">CLOSETS</option>
<option value="WASH BASINS">WASH BASINS</option>
<option value="WASH COUNTERS">WASH COUNTERS</option>
<option value="SHOWERS & PANELS">SHOWERS & PANELS</option>
<option value="FANCY MIRRORS">FANCY MIRRORS</option>
<option value="LUXURY BATHROOMS">LUXURY BATHROOMS</option>
<option disabled></option>
<option>FAUCETS & FITTINGS</option>
<option disabled></option>
<option>DESIGNER GLASS DOORS</option>
<option disabled></option>
<option>MODULAR KITCHEN</option>

</select>
</div>
    <div class="form-group">
  <label for="description">Description:</label>
  <textarea  class="ckeditor" id="description" name="description"></textarea>
  </div>
    <div class="form-group">
    <label for="image">Image:</label>
    <input type="file" class="btn btn-primary" id="image" name="image">
  </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>
</html>