<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>



<nav class="navbar navbar-inverse">
  <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo site_url(); ?>/manage-tilesandsurfaces">Manage Products</a></li>
        <li><a href="<?php echo site_url(); ?>/add-tilesandsurfaces">Add Products</a></li>
      </ul>
  </div>
</nav>


  <h2>Update PRODUCTS</h2>  
<form role="form" method="post" action="<?php echo site_url()?>/edit-tilesandsurfaces-post" enctype="multipart/form-data">

 <input type="hidden" value="<?php echo $tilesandsurfaces[0]->id ?>"   name="tilesandsurfaces_id">


    <div class="form-group">
<label for="category">Category:</label>
<select class="form-control" id="category" name="category">
    <option disabled>---TILES & SURFACES---</option>
<option value="INDOOR WALL TILES" <?php if($tilesandsurfaces[0]->category == "INDOOR WALL TILES"){ echo "selected"; } ?> >INDOOR WALL TILES</option>
<option value="INDOOR FLOOR TILES" <?php if($tilesandsurfaces[0]->category == "INDOOR FLOOR TILES"){ echo "selected"; } ?> >INDOOR FLOOR TILES</option>
<option value="OUTDOOR WALL TILES" <?php if($tilesandsurfaces[0]->category == "OUTDOOR WALL TILES"){ echo "selected"; } ?> >OUTDOOR WALL TILES</option>
<option value="OUTDOOR FLOOR TILES" <?php if($tilesandsurfaces[0]->category == "OUTDOOR FLOOR TILES"){ echo "selected"; } ?> >OUTDOOR FLOOR TILES</option>
<option value="LAWN & TERRACE TILES" <?php if($tilesandsurfaces[0]->category == "LAWN & TERRACE TILES"){ echo "selected"; } ?> >LAWN & TERRACE TILES</option>
<option value="WOODEN FLOORING" <?php if($tilesandsurfaces[0]->category == "WOODEN FLOORING"){ echo "selected"; } ?> >WOODEN FLOORING</option>
<option disabled>---BATHROOM ACCESSORIES---</option>
<option value="CLOSETS" <?php if($tilesandsurfaces[0]->category == "CLOSETS"){ echo "selected"; } ?>>CLOSETS</option>
<option value="WASH BASINS" <?php if($tilesandsurfaces[0]->category == "WASH BASINS"){ echo "selected"; } ?>>WASH BASINS</option>
<option value="WASH COUNTERS" <?php if($tilesandsurfaces[0]->category == "WASH COUNTERS"){ echo "selected"; } ?>>WASH COUNTERS</option>
<option value="SHOWERS & PANELS" <?php if($tilesandsurfaces[0]->category == "SHOWERS & PANELS"){ echo "selected"; } ?>>SHOWERS & PANELS</option>
<option value="FANCY MIRRORS" <?php if($tilesandsurfaces[0]->category == "FANCY MIRRORS"){ echo "selected"; } ?>>FANCY MIRRORS</option>
<option value="LUXURY BATHROOMS" <?php if($tilesandsurfaces[0]->category == "LUXURY BATHROOMS"){ echo "selected"; } ?>>LUXURY BATHROOMS</option>
<option disabled></option>
<option <?php if($tilesandsurfaces[0]->category == "FAUCETS & FITTINGS"){ echo "selected"; } ?>>FAUCETS & FITTINGS</option>
<option disabled></option>
<option <?php if($tilesandsurfaces[0]->category == "DESIGNER GLASS DOORS"){ echo "selected"; } ?>>DESIGNER GLASS DOORS</option>
<option disabled></option>
<option <?php if($tilesandsurfaces[0]->category == "MODULAR KITCHEN"){ echo "selected"; } ?>>MODULAR KITCHEN</option>
</select>
</div>
      <div class="form-group">
  <label for="description">Description:</label>
<textarea  class="ckeditor" id="description" name="description">     <?php echo $tilesandsurfaces[0]->description ?>       </textarea>
 </div>
      <div class="form-group">
    <label for="image">Image:</label>
    <input type="file" class="btn btn-primary" id="image" name="image">
  </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

</body>
</html>